<?php

use App\Http\Controllers\Api\V1\CommunityLinkController;
use App\Mail\MyTestMail;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'language'], function () {

Auth::routes(['verify' => 'true']);
Route::group(['middleware' => 'verified'], function () {

    Route::get('community', [App\Http\Controllers\CommunityLinkController::class, 'index']);
    Route::post('community', [App\Http\Controllers\CommunityLinkController::class, 'store']);
    Route::get('community/{channel}', [App\Http\Controllers\CommunityLinkController::class, 'index']);
    Route::post('votes/{link}',[App\Http\Controllers\CommunityLinkUserController::class,'store']);
    Route::get('/uploadAvatar',[\App\Http\Controllers\UserController::class, 'index']);
    Route::post('/uploadAvatar', [\App\Http\Controllers\UserController::class, 'upload']);


     Route::get('/home', function () {
        return view('welcome');
    }); 

});


Route::view('/','home');

});




/* Route::get('/', function () {
    return view('welcome');
}); */

Auth::routes();

/* Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home'); */




//GET

/*  Route::get('/community/user/{user_id?}', function ($user_id = null){


    return 'Hola, ha funcionado';
});  */

/* Route::get('/community/user/{user_id}', function ($user_id='Jose'){

    return $user_id;

}); */


//POST
/* Route::post('community/post', function(){

    return 'Hola, ha funcionado';

}); */

//MATCH

/* Route::match(['get','post'], '/community/getpost', function(){

    return 'Hola, estoy aqui';

}); */

//Comprobacion numeros

/* Route::get('community/user/{user}',function($user){

    return 'He funcionado';

})->where('user' , '[0-9]+' ); */

//Comprobacion solo letras y números

/* Route::get('community/user/{id}/{name}', function($id, $name)
{
    return 'Funciona';
})
->where(['id' => '[0-9]+', 'name' => '[a-z]+']); */



//Comprobar host

/* Route::get('/host', function(){

    $env= env('DB_HOST');

    return $env;
}); */

//Comprobar zona horaria

Route::get('/timezone', function(){

    $config= config('app.timezone');
    return $config;

});





