<?php

namespace App\Queries;

use App\Models\CommunityLink;
use App\Models\Channel;


class CommunityLinksQuery{


    public static function getByChannel(Channel $channel){


        return $channel->communityLinks()->where('channel_id',$channel->id)->where('approved',true)->latest('updated_at')->paginate(25);


    }

    public static function getAll(){

       return CommunityLink::where('approved',true)->latest('updated_at')->paginate(25);

    }

    public static function getMostPopular(){

        return CommunityLink::where('approved',true)->withCount('users')->orderBy('users_count','desc')->paginate(25);

    }

    public static function getMostPopularbySearch($search){


        $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY); 
           
        
        return CommunityLink::where(function ($q) use ($searchValues){

            
            foreach ($searchValues as $value){

                $q->where('title','like','%'.$value.'%');
            }

        })->where('approved',true)->withCount('users')->orderBy('users_count','desc')->paginate(25);

    }


}