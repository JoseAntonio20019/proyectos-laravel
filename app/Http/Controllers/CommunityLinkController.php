<?php

namespace App\Http\Controllers;

use App\Console\Commands\sendLink;
use App\Models\CommunityLink;
use App\Models\Channel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommunityLinkForm as RequestsCommunityLinkForm;
use App\Mail\MyTestMail;
use App\Models\CommunityLinkUser;
use App\Queries\CommunityLinksQuery;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CommunityLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Channel $channel = null)
    {


        if ($channel != null) {

            $links = CommunityLinksQuery::getByChannel($channel);
        } else if (request()->exists('search')) {

            if (request()->exists('popular')) {

                $search = trim(request()->get('search'));
                $links = CommunityLinksQuery::getMostPopularbySearch($search);
            } else {

                //$search=trim(request()->get('search'));
                $search = trim(request()->get('search'));

                $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);


                $links = CommunityLink::where(function ($q) use ($searchValues) {

                    foreach ($searchValues as $value) {


                        $q->where('title', 'like', '%' . $value . '%');
                    }
                })->where('approved', true)->paginate(25);
            }
        } else if (request()->exists('popular')) {

            $links = CommunityLinksQuery::getMostPopular();
            //$links=CommunityLink::where('approved','true')->withCount('users')->orderBy('users_count','desc')->get();
            //$links= CommunityLink::where('approved',true)->withCount('users')->orderBy('users_count','desc')->paginate(25);

            /* }else if(request()->exists('popular') && request()->has('search')){

            $search=request()->get('search');
            $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY); 
            $links=CommunityLink::where(function ($q) use ($searchValues){

            
                foreach ($searchValues as $value){
    
                    $q->where('title','like','%'.$value.'%');
                }
    
            });

           // $links=CommunityLinksQuery::getMostPopularandSearch($search); */
        } else {


            $links = CommunityLinksQuery::getAll();
        }

        $channels = Channel::orderBy('title', 'asc')->get();
        return view('community/index', compact('links', 'channels', 'channel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestsCommunityLinkForm $request)
    {

       /*  $this->validate($request, [
            'title' => 'required',
            'link' => 'required|active_url|',
            'channel_id' => 'required|exists:channels,id'
        ]); */

        $link = new CommunityLink();
        $link->user_id = Auth::id();

        $approved = Auth::user()->isTrusted() ? true : false;

        $request->merge(['user_id' => Auth::id(), 'approved' => $approved]);


        if ($approved) {

            if ($link->hasAlreadyBeenSubmitted($request->link)) {

                return back()->with('warning', 'The post is now updated, we will update the date. Thanks by post it!');
            } else {
                CommunityLink::create($request->all());
                return back()->with('success', 'Link created succesfully!');
            }
        } else {

            
           $details = [
                'title' => 'Mail from ItSolutionStuff.com',
                'body' => 'Verifica links, por favor.'
            ];
             Mail::to('j2o0s0e1@gmail.com')->send(new MyTestMail($details));
             return  back()->with('warning', 'Your user will be approved shortly!');
            
        }
    }








    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function edit(CommunityLink $communityLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommunityLink $communityLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityLink $communityLink)
    {
        //
    }

}
