<?php

namespace App\Http\Controllers;

use App\Http\Requests\UploadAvatarForm;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    
    public function index(){


        return view('partials.add-image');


    }


    public function upload(UploadAvatarForm $request){

            $request->file('avatar')->store('public');

            $user=Auth::user(); 
            $user->avatar= asset('storage/'.$request->file('avatar')->hashName());
            $user->save();

            dd($user);


            return redirect('/community');

        }


        }
    

