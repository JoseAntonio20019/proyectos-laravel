<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Channel;
use App\Models\CommunityLink;
use Illuminate\Http\Request;
use App\Queries\CommunityLinksQuery;
use App\Http\Requests\CommunityLinkForm;
use App\Mail\MyTestMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CommunityLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {

        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }


    public function index()
    {

        if (request()->exists('search')) {

            if (request()->exists('popular')) {

                $search = trim(request()->get('search'));
                $links = CommunityLinksQuery::getMostPopularbySearch($search);
            } else {

            
                $search = trim(request()->get('search'));

                $searchValues = preg_split('/\s+/', $search, -1, PREG_SPLIT_NO_EMPTY);


                $links = CommunityLink::where(function ($q) use ($searchValues) {

                    foreach ($searchValues as $value) {


                        $q->where('title', 'like', '%' . $value . '%');
                    }
                })->where('approved', true)->paginate(2);
            }
        } else if (request()->exists('popular')) {

            $links = CommunityLinksQuery::getMostPopular();
        } else {


            $links = CommunityLinksQuery::getAll();
        }

        return response()->json(['Links' => $links], 200);
        //return view('community/index', compact('links', 'channels', 'channel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommunityLinkForm $request)
    {


        $link = new CommunityLink();
        $link->user_id = Auth::id();

        $approved = Auth ::user()->isTrusted() ? true : false;

        $request->merge(['user_id' => Auth::id(), 'approved' => $approved]);


        if ($approved) {

            if ($link->hasAlreadyBeenSubmitted($request->link)) {

                return back()->with('warning', 'The post is now updated, we will update the date. Thanks by post it!');
            } else {
                CommunityLink::create($request->all());
                //return back()->with('success', 'Link created succesfully!');
            }
        } else {

            
           $details = [
                'title' => 'Mail from ItSolutionStuff.com',
                'body' => 'Verifica links, por favor.'
            ];
             Mail::to('j2o0s0e1@gmail.com')->send(new MyTestMail($details));
             //return  back()->with('warning', 'Your user will be approved shortly!');
            
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function show(CommunityLink $communityLink)
    {


        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommunityLink $communityLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommunityLink  $communityLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommunityLink $communityLink)
    {


            $link=CommunityLink::find($communityLink->id);
            $link->delete();

             return 'Link eliminado correctamente';
        
    }
}
