<?php

namespace App\Console\Commands;

use App\Mail\MyTestMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class sendLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendmail:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $details = [
            'title' => 'Mail from ItSolutionStuff.com',
            'body' => 'This is for testing email using smtp'
        ];
        
        Mail ::to('j2o0s0e1@gmail.com')->send(new MyTestMail($details));
    }
}
