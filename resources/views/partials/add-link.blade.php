<div class="card">
    <div class="card-header">
        <h3>{{__('Contribute a link')}}</h3>
    </div>
    <div class="card-body">
        <form method="POST" action="/community">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title">{{__('Title')}}:</label>

                <input type="text" class="form-control" id="title" name="title" placeholder="What is the title of your article?">

            </div>
            @error('title')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group">
                <label for="Channel">{{__('Channel')}}:</label>
                <select class="form-control @error('channel_id') is-invalid @enderror" name="channel_id">
                    <option selected disabled>Pick a Channel...</option>
                    @foreach ($channels as $channel)
                    <option value="{{ $channel->id }}" {{ old('channel_id') == $channel->id ? 'selected' : '' }}>
                        {{ $channel->title }}
                    </option>
                    @endforeach
                </select>
                @error('channel_id')
                <span class="text-danger">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-group">
                <label for="link">{{__('Link')}}:</label>
                <input type="text" class="form-control" id="link" name="link" placeholder="What is the URL?">

            </div>
            @error('link')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-group card-footer">
                <button class="btn btn-primary">{{__('Contribute Link')}}</button>
            </div>
        </form>
    </div>
</div>