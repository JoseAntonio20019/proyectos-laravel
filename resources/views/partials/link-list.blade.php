<h1><a class="h1community" href="/community">{{__('CommunityLinks')}}</a>
    @if($channel !=null)
    - {{$channel->slug}}

    @endif
</h1>

@if(count($links)==0)
<h2>No contributions yet.</h2>
@else


<ul class="nav d-flex flex-row">
    <li class="nav-item">
        <a class="nav-link {{request()->exists('popular') ? '' : 'disabled' }}" href="{{request()->url()}}">{{__('Most recent')}}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{request()->exists('popular') ? 'disabled' : '' }}" href="{{request()->fullUrlWithQuery(['popular' => ''])}}">{{__('Most popular')}}</a>
    </li>
</ul>
<ul>
    @foreach ($links as $link)


    <li class="lista d-flex flex-row ">
        <form method="POST" action="votes/{{ $link->id }}">
            {{ csrf_field() }}
            <button class="btn {{ Auth::check() && Auth::user()->votedFor($link) ? 'btn-success' : 'btn-secondary' }} ">
                {{$link->users()->count()}}
                <i class="fas fa-thumbs-up" width="50" height="50"></i>
            </button>
        </form>
        <span class="label label-default" style="background: {{ $link->channel->color }}">
            <a class="links" href="/community/{{ $link->channel->slug }}">{{ $link->channel->title }}</a>

        </span>
        <a  class="label label-default me- mt-3"href="{{$link->link}}" target="_blank">
            {{$link->title}}
        </a>

        <small class="text-center mt-3 ms-3">{{__('Contributed by')}}: {{$link->creator->name}} {{$link->updated_at->diffForHumans()}}</small>
        
    </li>
    @endforeach
</ul>
@endif
{{ $links->links() }}

